var chart;
            var chartData = [
                {
                    "day": '01/09',
                     "send": 5000,
                    "delivered": 600,
                    "not_delivered": 2000
                },
                {
                    "day": '02/09',
                     "send": 8000,
                    "delivered": 2000,
                    "not_delivered": 4000
                },
                {
                    "day": '03/09',
                     "send": 7000,
                    "delivered": 3000,
                    "not_delivered": 700
                },
                {
                    "day": '04/09',
                    "send": 7000,
                    "delivered": 3000,
                    "not_delivered": 700
                },
                {
                    "day": '05/09',
                    "send": 5000,
                    "delivered": 900,
                    "not_delivered": 1000,
                    "dashLengthLine": 5
                },
                {
                    "day": '06/09',
                    "send": 5000,
                    "delivered": 900,
                    "not_delivered": 1000,
                    "dashLengthLine": 5
                }
                // {
                //     "day": 01/09,
                //     "income": 34.1,
                //     "expenses": 29.9,
                //     "dashLengthColumn": 5,
                //     "alpha":0.2,
                //     "additional":"(projection)"
                // }

            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();

                chart.dataProvider = chartData;
                chart.categoryField = "day";
                chart.startDuration = 1;
                chart.addTitle("График за выбранный период", 12);

                // chart.handDrawn = true;
                // chart.handDrawnScatter = 3;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.dashLength = 5;
                valueAxis.title = "";
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // column graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "Отправлено";
                graph1.lineColor = "#f3e145";
                graph1.valueField = "send";
                graph1.lineAlpha = 0;
                graph1.fillAlphas = 1;
                graph1.bulletBorderColor = "#f3e145";
                graph1.dashLengthField = "dashLengthColumn";
                graph1.alphaField = "alpha";
                graph1.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph1);

                // line
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "column";
                graph2.title = "Доставлено";
                graph2.lineColor = "#0d9f67";
                graph2.valueField = "delivered";
                graph2.lineAlpha = 1;
                graph2.fillAlphas = 1;
                // graph2.bullet = "round";
                // graph2.bulletBorderThickness = 3;
                graph2.bulletBorderColor = "#0d9f67";
                graph2.bulletBorderAlpha = 1;
                graph2.bulletColor = "#ffffff";
                graph2.dashLengthField = "dashLengthColumn";
                graph2.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph2);

                // line
                var graph3 = new AmCharts.AmGraph();
                graph3.type = "column";
                graph3.title = "Не доставлено";
                graph3.lineColor = "#2f98da";
                graph3.valueField = "not_delivered";
                graph3.lineAlpha = 1;
                graph3.fillAlphas = 1;
                // graph2.bullet = "round";
                // graph2.bulletBorderThickness = 3;
                graph3.bulletBorderColor = "#2f98da";
                graph3.bulletBorderAlpha = 1;
                graph3.bulletColor = "#ffffff";
                graph3.dashLengthField = "dashLengthColumn";
                graph3.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph3);


                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.useGraphSettings = true;
                chart.addLegend(legend);

               
                // WRITE
                chart.write("chartdiv");
            });