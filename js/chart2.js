var chart2;
            var chartData2 = [
                {
                    "day": '01/09',
                    "send2": 1000,
                    "delivered2": 600,
                    "not_delivered": 2000
                },
                {
                    "day": '02/09',
                    "send2": 8000,
                    "delivered2": 2000,
                    "not_delivered": 1000
                },
                {
                    "day": '03/09',
                    "send2": 7000,
                    "delivered": 3000,
                    "not_delivered": 700
                },
                {
                    "day": '04/09',
                    "send2": 7000,
                    "delivered2": 3000,
                    "not_delivered": 2200
                },
                {
                    "day": '05/09',
                    "send2": 5000,
                    "delivered2": 6000,
                    "not_delivered": 2000,
                    "dashLengthLine": 5
                },
                {
                    "day": '06/09',
                    "send2": 5000,
                    "delivered2": 900,
                    "not_delivered": 4000,
                    "dashLengthLine": 5
                }
                // {
                //     "day": 01/09,
                //     "income": 34.1,
                //     "expenses": 29.9,
                //     "dashLengthColumn": 5,
                //     "alpha":0.2,
                //     "additional":"(projection)"
                // }

            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart2 = new AmCharts.AmSerialChart();

                chart2.dataProvider = chartData2;
                chart2.categoryField = "day";
                chart2.startDuration = 1;
                chart2.addTitle("График за выбранный период", 12);

                // chart.handDrawn = true;
                // chart.handDrawnScatter = 3;

                // AXES
                // category
                var categoryAxis2 = chart2.categoryAxis;
                categoryAxis2.gridPosition = "start";

                // value
                var valueAxis2 = new AmCharts.ValueAxis();
                valueAxis2.dashLength = 5;
                valueAxis2.title = "";
                valueAxis2.axisAlpha = 0;
                chart2.addValueAxis(valueAxis2);

                // GRAPHS
                // column graph
                var graph4 = new AmCharts.AmGraph();
                graph4.type = "column";
                graph4.title = "Отправлено";
                graph4.lineColor = "#f3e145";
                graph4.valueField = "send2";
                graph4.lineAlpha = 0;
                graph4.fillAlphas = 1;
                graph4.bulletBorderColor = "#f3e145";
                graph4.dashLengthField = "dashLengthColumn";
                graph4.alphaField = "alpha";
                graph4.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart2.addGraph(graph4);

                // line
                var graph5 = new AmCharts.AmGraph();
                graph5.type = "column";
                graph5.title = "Доставлено";
                graph5.lineColor = "#0d9f67";
                graph5.valueField = "delivered2";
                graph5.lineAlpha = 1;
                graph5.fillAlphas = 1;
                // graph2.bullet = "round";
                // graph2.bulletBorderThickness = 3;
                graph5.bulletBorderColor = "#0d9f67";
                graph5.bulletBorderAlpha = 1;
                graph5.bulletColor = "#ffffff";
                graph5.dashLengthField = "dashLengthColumn";
                graph5.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart2.addGraph(graph5);

                // line
                var graph6 = new AmCharts.AmGraph();
                graph6.type = "column";
                graph6.title = "Не доставлено";
                graph6.lineColor = "#2f98da";
                graph6.valueField = "not_delivered";
                graph6.lineAlpha = 1;
                graph6.fillAlphas = 1;
                // graph2.bullet = "round";
                // graph2.bulletBorderThickness = 3;
                graph6.bulletBorderColor = "#2f98da";
                graph6.bulletBorderAlpha = 1;
                graph6.bulletColor = "#ffffff";
                graph6.dashLengthField = "dashLengthColumn";
                graph6.balloonText = "<span style='font-size:13px;'><b>[[value]]</b> [[additional]]</span>";
                chart2.addGraph(graph6);


                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.useGraphSettings = true;
                chart2.addLegend(legend);

               
                // WRITE
                chart2.write("chart2");
            });